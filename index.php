<?php   
    require_once 'vendor/autoload.php';
    require_once 'App/Core/Autoload.php';
    require_once 'App/Config/Constants.php';
    require_once 'App/Config/Routes.php';
    require_once 'App/Config/Database.php';  
    //use App\Interfaces\Object;  
    use App\Core\Router;
    
    $router = Router::getInstance();
    $router->parse();
?>