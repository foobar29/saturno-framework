<?php 
    /**
     *  Carga automáticamente las clases que son referenciadas en el proyecto.
     */
    spl_autoload_register(function($class){
        $route = str_replace('\\','/', $class).'.php';
        if(is_readable($route)) require_once($route);
    });
?>