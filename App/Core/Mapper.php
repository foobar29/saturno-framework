<?php namespace App\Core;

    /**
     *  --------------------
     *  Mapper
     *  --------------------
     *  Capa de consulta a base de datos.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Mapper
        {   
            protected $database;
            
            /**
             *  Constructor de clase
             *  Accede a una instancia de la conexión de la base de datos.
             *  @access public
             * 
             *  @return void
             */
            protected function __construct()
                {
                    $db = DatabaseConnection::getInstance();
                    $this->database = $db->handleConnection();
                }
        }
?>