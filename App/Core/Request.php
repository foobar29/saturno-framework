<?php namespace App\Core;

    use App\Interfaces\Request as I_Request;

    /**
     *  --------------------
     *  Request
     *  --------------------
     *  Gestiona los valores recibidos en los distintos tipos de requests.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
   final class Request implements I_Request
        {
            private static $instance;

            /**
             *  Constructor de clase
             */
            private function __construct(){}

            /**
             *  Obtener instancia
             *  Retorna una instancia de sí misma.
             *  @access public
             * 
             *  @return Request
             * 
             *  @example $foo = Request::getInstance(); 
             */
            public static function getInstance():Request
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }
            
            /**
             * 
             */
            public function get(string $elem = null)
                {
                    if($elem) return $_GET[$elem];
                    else return $_GET;
                }

            /**
             * 
             */
            public function post(string $elem = null)
                {
                    if($elem) return $_POST[$elem];
                    else return $_POST;
                }

            /**
             * 
             */
            public function put(string $elem = null)
                {
                    if($elem) return $_PUT[$elem];
                    else return $_PUT;
                }
            
            /*
             * 
            public static function getData(string $type_of_request, string $elem=null):I_Request
                {
                    switch($type_of_request)
                        {
                            case 'get':
                                break;
                            case 'post':
                                break;
                            case 'put':
                                break;
                            case 'delete':
                                break;
                            case 'session':
                                break;
                            case 'cookie':
                                break;
                            case 'env':
                                break;
                            case 'files':
                                break;
                            case 'request':
                                break;
                            case 'server':
                                break;
                        }
                }*/
        }
?>