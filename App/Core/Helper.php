<?php namespace App\Core;

    /**
     *  --------------------
     *  Helper
     *  --------------------
     *  Carga los helpers para ser usados en los controladores.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
   final class Helper extends Loader
        {
            private static $instance;
            private $virtual_file;
            /**
             *  Constructor de clase
             *  Apunta el directorio raíz de donde se cargarán los helpers.
             *  @access private
             * 
             *  @return void
             */
            private function __construct()
                {
                    $this->setPath(implode('/', [SYSTEM_PATH,HELPERS_PATH]));
                    $this->setExtension('php');
                    $this->virtual_file=[];
                }

            private function addFile(string $filedata)
                {
                    $this->virtual_file[]=$filedata;
                }
            
            public function parseFiles()
                {
                    foreach($this->virtual_file as $file) eval('?>'. $file .'<?php ');
                }

            /**
             *  Obtener instancia
             *  Retorna una instancia de la clase.
             *  @access public
             * 
             *  @return Helper
             * 
             *  @example $foo = Helper::getInstance();
             */
            public static function getInstance():Helper
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }      
            
            /**
             *  Cargar
             *  Carga el/los helper(s) especificados.
             *  @access public
             * 
             *  @param string|array $file
             *  @return void
             * 
             *  @example $foo->load(['testing', 'maths']);
             */
            public function load($file)
                {
                    if(is_array($file)) foreach($file as $element) 
                        {
                            $this->addFile($this->requireElement($element, true));
                        }
                    else if(is_string($file)) $this->addFile($this->requireElement($file, true));
                    $this->parseFiles();
                }
        }
?>