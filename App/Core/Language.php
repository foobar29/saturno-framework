<?php namespace App\Core;

    /**
     *  -------------------
     *  Language
     *  -------------------
     *  Carga los archivos de lenguaje para ser utilizados en las vistas desplegadas por los controladores.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
   final class Language extends Loader
        {
            private static $instance;
            private $elements;

            /**
             *  Constructor de clase
             *  Apunta el directorio raíz de donde se cargarán los archivos de lenguaje.
             *  @access private
             * 
             *  @return void
             */
            private function __construct()
                {
                    $this->setPath(implode('/',[SYSTEM_PATH,LANGUAGES_PATH]));
                    $this->setExtension('lang');
                    $this->setSubpath('spanish');
                    $this->elements=[];
                }
            
            /**
             *  Obtener instancia
             *  Retorna una instancia de la clase.
             *  @access public
             * 
             *  @return Language
             * 
             *  @example $foo = Language::getInstance(); 
             */
            public static function getInstance():Language
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }
            
            /**
             *  Asignar lenguaje
             *  Apunta al subdirectorio del idioma del que se desee cargar un archivo de lenguaje.
             *  @access public
             * 
             *  @param string $lang
             *  @return void
             * 
             *  @example $foo->setLanguage('spanish');
             */
            public function setLanguage(string $lang)
                {
                    $this->setSubpath($lang);
                }
            
            /**
             *  Asignar elementos
             *  Recibe el conjunto de elementos accesibles.
             *  @access private
             * 
             *  @param Object $elements
             *  @return void
             * 
             *  @example $this->setElements('errores',$lang_obj);
             */
            private function setElements(string $key, Object $elements)
                {
                    $this->elements[$key]=$elements;
                }

            /**
             *  Obtener elemento
             *  Retorna el valor del elemento especificado.
             *  @access public
             * 
             *  @param string $elem
             *  @return string|void
             * 
             *  @example $foo->getElement('main', 'title');
             */
            public function getElement(string $file, string $key):?string
                {
                    return $this->elements[$file]->$key;
                }
            
            /**
             *  Cargar
             *  Carga el/los archivo(s) de lenguaje especificados.
             *  @access public
             * 
             *  @param string|array $file
             *  @return void
             * 
             *  @example $foo->load(['principal', 'errores']);
             */
            public function load($file)
                {
                    if(is_array($file)) foreach($file as $element) $this->setElements($element,json_decode($this->requireElement($element, true)));
                    else if(is_string($file)) $this->setElements($file,json_decode($this->requireElement($file, true)));       
                }

        }
?>