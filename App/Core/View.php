<?php namespace App\Core;

    /**
     *  --------------------
     *  View
     *  --------------------
     *  Carga las vistas para ser desplegadas en los controladores.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    final class View extends Loader
        {
            private static $instance;
            protected $resource;
            protected $language;
            protected $data;
            
            /**
             *  Constructor de clase
             *  Apunta al directorio raíz donde se cargarán las vistas.
             *  @access private
             * 
             *  @return void
             */
            private function __construct()
                {
                    $this->resource = Resource::getInstance();
                    $this->language = Language::getInstance();
                    $this->setPath(implode('/',[SYSTEM_PATH,VIEWS_PATH]));
                    $this->setExtension('php');
                }

            /**
             *  Obtener instancia
             *  Retorna una instancia de la clase
             *  @access public
             *  
             *  @return View
             * 
             *  @example $foo = View::getInstance();
             */
            public static function getInstance():View
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }
            
            /**
             *  Asignar módulo
             *  Apunta al módulo/subdirectorio base donde se cargarán las vistas.
             *  @access public
             * 
             *  @param string $module
             *  @return void
             * 
             *  @example $foo->setModule('bar');
             */
            public function setModule(string $module)
                {
                    $this->setSubpath($module);
                }

            /**
             *  Cargar
             *  Carga el/las vista(s) especificada(s).
             *  @access public
             * 
             *  @param string|array $file
             *  @param array|void $data
             *  @return void
             * 
             *  @example $foo->load(['principal', 'errores'], ['foo' => 123, 'bar'=> 456]);
             */
            final public function load($file, $data=null)
                {
                    $this->data=$data;
                    if(is_array($file)) foreach($file as $element) $this->requireElement($element, false);
                    else if(is_string($file)) $this->requireElement($file, false);  
                }
        
        }
?>