<?php namespace App\Core;

    /**
     *  --------------------
     *  Resource
     *  --------------------
     *  Carga los recursos JS y CSS para ser referenciados en las vistas.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    final class Resource extends Loader
        {
            private static $instance;
            private $js_elements;
            private $css_elements;

            /**
             *  Constructor de clase
             *  Apunta el directorio raíz de donde se cargarán los recursos JS y CSS.
             *  @access private
             * 
             *  @return void
             */
            private function __construct()
                {
                    $this->setPath(implode('/',[BASE_PATH,RESOURCES_PATH]));
                }

            /**
             *  Obtener instancia de la función
             *  Retorna una instancia de sí misma.
             *  @access public
             *
             *  @return Resource
             * 
             *  @example $foo = Resource::getInstance();
             */
            public static function getInstance():Resource
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }

            /**
             *  Asignar módulo
             *  Apunta al módulo/subdirectorio base donde se cargarán los recursos.
             *  @access public
             * 
             *  @param string $module
             *  @return void
             * 
             *  @example $foo->setModule('administrador');
             */
            public function setModule(string $module)
                {
                    $this->setSubpath($module);
                }

            /**
             *  Cargar CSS
             *  Añade y parsea el/los elemento(s) CSS a un formato legible por la vista.
             *  @access public
             * 
             *  @param $elements
             *  @return void
             * 
             *  @example $foo->loadCSS('styles');
             */
            public function loadCSS($elements)
                {
                    if(is_array($elements)) foreach($elements as $element) $this->formatCSS($element);
                    else if(is_string($elements)) $this->formatCSS($elements);
                }

            /**
             *  Cargar JS
             *  Añade y parsea el/los elemento(s) JS a un formato legible por la vista.
             *  @access public
             * 
             *  @param $elements
             *  @return void
             * 
             *  @example $foo->loadJS('main');
             */
            public function loadJS($elements)
                {
                    if(is_array($elements)) foreach($elements as $element) $this->formatJS($element);
                    else if(is_string($elements)) $this->formatJS($elements);
                }
            
            /**
             *  Formatear JS
             *  Parsea el elemento JS especificado a un formato legible por la vista.
             *  @access private
             * 
             *  @param string $element
             *  @return void
             * 
             *  @example $foo->formatJS('main');
             */
            private function formatJS(string $element)
                {
                    $this->js_elements.='<script src="'.implode('/',[$this->getPath(),$this->getSubpath(),'js',$element]).'.js"></script>';
                }

            /**
             *  Formatear CSS
             *  Parsea el elemento CSS especificado a un formato legible por la vista.
             *  @access private
             *  
             *  @param string $element
             *  @return void
             * 
             *  @example $foo->formatCSS('styles');
             */
            private function formatCSS(string $element)
                {
                    $this->js_elements.='<link href="'.implode('/',[$this->getPath(),$this->getSubpath(),'css',$element]).'.css">';
                }

            /**
             *  Obtener CSS
             *  Devuelve el conjunto de elementos CSS ya formateados.
             *  @access public
             * 
             *  @return string|void
             * 
             *  @example $foo->getCSS();
             */
            public function getCSS():?string
                {
                    return $this->css_elements;
                }
            
            /**
             *  Obtener JS
             *  Devuelve el conjunto de elementos JS ya formateados.
             *  @access public
             * 
             *  @return string|void
             * 
             *  @example $foo->getJS();
             */
            public function getJS():?string
                {
                    return $this->js_elements;
                }
        
        }
?>