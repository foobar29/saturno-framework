<?php namespace App\Core;
    
    use App\Models\Entities\Route;

    /**
     *  --------------------
     *  Router
     *  --------------------
     *  Gestiona las peticiones recibidas y las vincula a la ejecución de una clase-método.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    final class Router
        {
            private $segments;
            private static $instance;
            private $elements;

            /**
             *  Constructor de clase
             *  Captura la url de la petición realizada y la deconstruye para su posterior análisis.
             *  @access private
             * 
             *  @return void
             */
            private function __construct()
                {
                    $this->elements=[];
                    $request = parse_url($_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], PHP_URL_PATH);
                    $request_filtered = str_replace($_SERVER['SERVER_NAME'].'/'. BASE_PATH, '', $request);
                    $this->segments = explode("/",$request_filtered);
                }
            
            /**
             *  Obtener instancia
             *  Retorna una instancia de sí misma.
             *  @access public
             * 
             *  @return Router
             * 
             *  @example $foo = Router::getInstance();
             */
            public static function getInstance():Router
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }
            
            /**
             *  Enlazar
             *  Vincula una ruta de acceso con otra.
             *  @access public
             * 
             *  @return Router
             * 
             *  @example $foo->link(new Route('admin', 'inicio','get'), new Route('administrador','index'));
             */
            public function link(Route $from, Route $to):Router
                {
                    $this->elements[]=['from'=>$from,'to'=>$to];
                    return $this;
                }
                
            /**
             *  Buscar en ruta 'desde'
             *  Busca en la colección de rutas enlazadas todas las coincidencias que hayan respecto al parámetro especificado. 
             *  @access private
             * 
             *  @param Route $route
             *  
             *  @return array|void
             */
            private function findFrom(Route $route):?array
                {
                    $find = array_filter($this->elements, function($elem) use ($route){ 
                        return (  
                                   $elem['from']->getController()==$route->getController()&&
                                   $elem['from']->getMethod()==$route->getMethod()&& 
                                   in_array($route->getTypeOfRequest()[0],$elem['from']->getTypeOfRequest()) 
                                ); });
                    return $find;
                }
            
            /**
             *  Buscar en ruta 'hasta'
             *  Busca en la colección de rutas enlazadas todas las coincidencias que hayan respecto al parámetro especificado. 
             *  @access private
             * 
             *  @param Route $route
             *  
             *  @return array|void
             */
            private function findTo(Route $route):?array
                {
                    $find = array_filter($this->elements, function($elem) use ($route){ 
                             return (   
                                        $elem['to']->getController()==$route->getController()&&
                                        $elem['to']->getMethod()==$route->getMethod()&& 
                                        in_array($route->getTypeOfRequest()[0],$elem['to']->getTypeOfRequest())  
                                     ); });
                    return $find;
                }
            
            /**
             *  Parsear
             *  Traduce la url de la petición realizada en una ruta reconocible por la aplicación. 
             *  Inicializa un controlador y ejecuta un método que coincida con el criterio de la ruta generada.
             *  @access public
             * 
             *  @return void
             * 
             *  @example $foo->parse();
             */
            public function parse()
                {
                    if(empty($this->segments[1]))$this->segments[1] = DEF_CONTROLLER;
                    $customRoute=$this->findFrom(new Route($this->segments[1],(empty($this->segments[2])?null:$this->segments[2]),strtolower($_SERVER['REQUEST_METHOD'])));
                    if($customRoute!=[]){$this->segments[1]=reset($customRoute)['to']->getController();$this->segments[2]=reset($customRoute)['to']->getMethod();}
                    $namespace='\\'.SYSTEM_PATH.'\\'.CONTROLLERS_PATH.'\\'.ucfirst(strtolower($this->segments[1]));
                    if (is_readable(preg_replace('/\//','',str_replace('\\','/',$namespace),1).'.php'))
                        {
                            $controller = new $namespace;
                            if(empty($this->segments[2])) $this->segments[2] = DEF_METHOD;
                            if(method_exists($controller , $this->segments[2])) $controller->{$this->segments[2]}((!empty($this->segments[3])?$this->segments[3]:null));        
                            else header('Location: ../'.DEF_CONTROLLER_WHEN_CNF);             
                        }
                    else
                        {
                            if(empty($this->segments[2])) header('Location: '.DEF_CONTROLLER_WHEN_CNF); 
                            else header('Location: ../'.DEF_CONTROLLER_WHEN_CNF);
                        } 
                }
        }
?>