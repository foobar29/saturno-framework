<?php namespace App\Core;
    
    /**
     *  --------------------
     *  Controller
     *  --------------------
     *  Carga el conjunto de componentes del Core requeridos para el funcionamiento general de los controladores.
     *  Requiere ser extendido por todos los controladores.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Controller
        {
            protected $view;
            protected $helper;
            protected $resource;
            protected $language;
            protected $database;
            protected $request;

            /**
             *  Constructor de clase
             *  Se accede a la instancia global de cada componente.
             *  @access public
             *  
             *  @return void
             */
            public function __construct()
                {
                    $this->view     =       View::getInstance();
                    $this->helper   =       Helper::getInstance();
                    $this->resource =       Resource::getInstance();
                    $this->language =       Language::getInstance();
                    $this->database =       DatabaseConnection::getInstance();
                    $this->request =        Request::getInstance();
                    $module=str_replace(implode('\\',[SYSTEM_PATH,CONTROLLERS_PATH]).'\\','',get_called_class());
                    $this->view->setModule($module);
                    $this->resource->setModule($module);
                }
        }
?>