<?php namespace App\Core;
    
    use App\Models\Entities\Credentials;

    /**
     *  --------------------
     *  DatabaseConnection
     *  --------------------
     *  Factory de objetos PDO.
     *  Crea conexiones a bases de datos y permite que estas sean accedidas con PDO.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    final class DatabaseConnection
        {
            private $credentials;
            private $dbms;
            private $connection;
            private static $instance;
            
            /**
             *  Destructor de clase
             *  Cierra la conexión a la base de datos automáticamente.
             *  @access public
             * 
             *  @return void
             */
            public function __destruct()
                {
                    $this->close();
                }
            
            /**
             *  Obtener instancia
             *  Retorna una instancia de la clase.
             *  @access public
             * 
             *  @return DatabaseConnection
             * 
             *  @example $foo = DatabaseConnection::getInstance();
             */
            public static function getInstance():DatabaseConnection
                {
                    if(!self::$instance instanceof self) self::$instance = new self;
                    return self::$instance;
                }
            
            /**
             *  Asignar credenciales
             *  Recibe las credenciales de acceso a la base de datos.
             *  @access public
             *
             *  @param Credentials $credentials
             *  @return DatabaseConnection
             * 
             *  @example $foo->setCredentials(new Credentials($host,$dbname,$user,$password));
             */
            public function setCredentials(Credentials $credentials):DatabaseConnection
                {
                    $this->credentials = $credentials;
                    return $this;
                }

            /**
             *  Asignar gestor de base de datos
             *  Recibe el gestor de base de datos.
             *  @access public
             * 
             *  @param string $dbms
             *  @return DatabaseConnection
             * 
             *  @example $foo->setDBMS('mysql');
             */
            public function setDBMS(string $dbms):DatabaseConnection
                {
                    $this->dbms = $dbms;
                    return $this;
                }
            
            /**
             *  Conectar
             *  Inicializa la conexión con la base de datos.
             *  @access public
             *  
             *  @return DatabaseConnection
             * 
             *  @example $foo->connect();
             */
            public function connect():DatabaseConnection
                {
                    try
                        {
                            $this->connection = new \PDO($this->dbms.':host='.$this->credentials->getHost().';dbname='.$this->credentials->getDatabase().';',$this->credentials->getUser(),$this->credentials->getPassword());
                        }
                    catch(PDOException $e)
                        {
                            throw new Exception($e);
                        }
                    return $this;
                }
            
            /**
             *  Manejar conexión
             *  Retorna la conexión PDO.
             *  @access public
             * 
             *  @return PDO|void
             * 
             *  @example $dbInstance = $foo->handleConnection();
             */
            public function handleConnection():?\PDO
                {
                    return $this->connection;
                }

            /**
             *  Cerrar
             *  Cierra la conexión PDO.
             *  @access public
             * 
             *  @return void
             * 
             *  @example $foo->close();
             */
            public function close()
                {
                    $this->connection = null;
                }

            
        }

?>