<?php namespace App\Core;

    /**
     *  --------------------
     *  Entity
     *  --------------------
     *  Añade una capa de funcionalidad a las entidades.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Entity
        {   
            protected $database;

            /**
             *  Constructor de clase
             *  Accede a una instancia de la conexión de la base de datos.
             *  @access protected
             * 
             *  @return void
             */
            protected function __construct()
                {
                    $db = DatabaseConnection::getInstance();
                    $this->database = $db->handleConnection();
                }
        }
?>