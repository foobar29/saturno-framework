<?php namespace App\Core;

    /**
     * --------------------
     *  Loader
     * --------------------
     *  Permite cargar distintos componentes de la aplicación: helpers, archivos de lenguaje, librerías, modelos y vistas.
     * 
     *  @package	Saturno
     *  @category   Core
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Loader
        {
            private $path       = '';
            private $subpath    = '';
            private $extension  = '';

            private function __construct(){}


            /**
             *  Asignar subdirectorio
             *  Apunta al subdirectorio especificado.
             *  @access protected
             * 
             *  @param string $path
             *  @return void
             * 
             *  @example $this->setSubpath('bar');
             */
            protected function setSubpath(string $path)
                {
                    $this->subpath = $path;
                }

            /**
             *  Obtener subdirectorio
             *  Devuelve el nombre del subdirectorio asignado.
             *  @access protected
             * 
             *  @return string|void
             * 
             *  @example $this->getSubpath();
             */
            protected function getSubpath():?string
                {
                    return $this->subpath;
                }

            /**
             *  Asignar directorio
             *  Apunta al directorio especificado.
             *  @access protected
             * 
             *  @param string $path
             *  @return void
             * 
             *  @example $this->setPath('foo');
             */
            protected function setPath(string $path)
                {
                    $this->path = $path;
                }

            /**
             *  Obtener directorio
             *  Devuelve el nombre del directorio asignado.
             *  @access protected
             * 
             *  @return string|void
             *  
             *  @example $this->getPath();
             */
            protected function getPath():?string
                {
                    return $this->path;
                }

            /**
             *  Obtener extensión
             *  Devuelve la extensión de fichero especificada.
             *  @access protected
             * 
             *  @return string|void
             *  
             *  @example $this->getExtension();
             */
            protected function getExtension():?string
                {
                    return $this->extension;
                }

            /**
             *  Asignar extensión
             *  Especifica la extensión del fichero.
             *  @access protected
             * 
             *  @param string $ext
             *  @return void
             * 
             *  @example $this->setExtension('php');
             */
            protected function setExtension(string $ext)
                {
                    $this->extension = $ext;
                }

            
            /**
             *  Requerir elemento
             *  Construye la ruta generada y busca el elemento solicitado, al encontrarlo inyecta su contenido.
             *  @access protected
             * 
             *  @param string $element
             *  @param bool $return
             * 
             *  @return string|void
             */
            protected function requireElement(string $element, bool $return=false):?string
                {
                    $route=($this->getSubpath()!=''?implode('/',[$this->getPath(),$this->getSubpath(),$element.'.'.$this->getExtension()]):implode('/',[$this->getPath(),$element.'.'.$this->getExtension()]));
                    if(is_readable($route)) if($return) return file_get_contents($route); 
                                            else { require_once($route); return null;}
                    else return null;        
                }
        }
?>