<?php namespace App\Controllers;
    use App\{Core\Controller as Controller, Interfaces\Controller as I_Controller};

    /**
     *  Error404
     *  Es llamado de forma predeterminada cuando una petición recibida no puede ser procesada.
     * 
     *  @package    Saturno
     *  @category   Controller
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Error404 extends Controller implements I_Controller
        {
            /**
             * Despliega vista predeterminada de error. 
             */
            public function index()
                {
                    $this->language->load('Errors');
                    $this->view->load('default');
                }
        } 

?>