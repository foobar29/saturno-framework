<?php namespace App\Controllers;
    use App\{Core\Controller as Controller, Interfaces\Controller as I_Controller, Models\Services\Account as AccountService};

    /**
     *  Testing
     *  Pruebas de funcionamiento.
     * 
     *  @package    Saturno
     *  @category   Controller
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Testing extends Controller implements I_Controller
        {
            public function index()
                {
                    $this->language->load(['testing', 'errors']); 
                }

            public function resources()
                {
                    
                }

            public function services()
                {
                    $accountService = new AccountService;
                    echo $accountService->login('jesus_cantu_290796@outlook.fr', 'passworddeprueba');
                }

            public function helpers()
                {
                    $this->helper->load('testing');
                    echo sumaDeValores(5,4);
                }

            public function languages()
                {
                    $this->language->load(['testing', 'errors']);
                    echo $this->language->getElement('testing', 'title');
                }

            public function views()
                {
                    $this->view->setModule('home');
                    $this->view->load('landpage');
                }

            public function database()
                {
                    $pdo = $this->database->handleConnection();
                    print_r($pdo);
                }
        }
?>