<?php namespace App\Controllers;
    use App\{Core\Controller as Controller, Interfaces\Controller as I_Controller};

    /**
     *  Home
     *  Es llamado de forma predeterminada cuando una petición apunta a la url raíz del proyecto
     * 
     *  @package    Saturno
     *  @category   Controller
     *  @author     Jesús Cantú <hola@jesuscantu.com.mx>
     *  @version    1.0.0.1
     */
    class Home extends Controller implements I_Controller
        {
            /**
             * Despliega la vista principal de bienvenida
             */
            public function index($params = null)
                {
                    $this->language->load('Main');
                    $this->view->load('landpage');
                }
        }
?>