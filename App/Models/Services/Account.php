<?php namespace App\Models\Services; use App\Models\{ Entities\Account as E_Account, Mappers\Account as M_Account };

    class Account
        {
            private $accountMapper;

            public function __construct()
                {
                    $this->accountMapper = new M_Account();
                }

            public function login($email, $password):?E_Account
                {
                    if($this->accountMapper->login($email, $password)) return new E_Account($email, $password);
                    else return null;
                }
        }
?>