<?php namespace App\Models\Entities;
    
    use App\Core\Entity;

    /**
     * @package	Saturno
     * @author Jesús Cantú
     * @version 1.0.0.1
     */
    class Credentials extends Entity
        {
            private $user;
            private $host;
            private $dbname;
            private $password;

            public function __construct(string $host=null, string $dbname=null, string $user=null, string $password=null)
                {
                    if($host)       $this->setHost($host);
                    if($dbname)     $this->setDatabase($dbname);
                    if($user)       $this->setUser($user);
                    if($password)   $this->setPassword($password);
                }

            /** 
             * 
             */
            public function setUser(string $user)
                {
                    $this->user = $user;
                }
            
            /**
             * 
             */
            public function setHost(string $host)
                {
                    $this->host = $host;
                }
            
            /**
             * 
             */
            public function setDatabase(string $dbname)
                {
                    $this->dbname = $dbname;
                }
            
            /**
             * 
             */
            public function setPassword(string $password)
                {
                    $this->password = $password;
                }
            
            /**
             * 
             */
            public function getUser():?string
                {
                    return $this->user;
                }
            
            /**
             * 
             */
            public function getHost():?string
                {
                    return $this->host;
                }
            
            /**
             * 
             */
            public function getDatabase():?string
                {
                    return $this->dbname;
                }

            /**
             * 
             */
            public function getPassword():?string
                {
                    return $this->password;
                }
        }
?>