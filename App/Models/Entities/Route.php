<?php namespace App\Models\Entities;

    use App\Core\Entity;

    /**
     * @package	Saturno
     * @author Jesús Cantú
     * @version 1.0.0.1
     */
    class Route extends Entity
        {
            private $controller;
            private $method;
            private $type_of_request;

            /**
             * 
             */
            public function __construct(string $controller=null, string $method=null, $type_of_request='get')
                {
                    parent::__construct();
                    $this->init($controller,$method,$type_of_request);
                }

            /**
             * 
             */
            public function init(string $controller=null, string $method=null, $type_of_request='get')
                {
                    $this->setController($controller);
                    $this->setMethod($method);
                    $this->setTypeOfRequest($type_of_request);
                }

            /**
             * 
             */
            public function setTypeOfRequest($type_of_request)
                {
                    if(is_array($type_of_request)) foreach($type_of_request as $elem) $this->type_of_request[]=$elem;
                    if(is_string($type_of_request))  $this->type_of_request[]=$type_of_request;
                }
                
            /**
             * 
             */
            public function getTypeOfRequest():?array
                {
                    return $this->type_of_request;
                }

            /**
             * 
             */
            public function setController($controller)
                {
                    $this->controller=$controller;
                }

            /**
             * 
             */
            public function setMethod($method)
                {
                    $this->method=$method;
                }
            
            /**
             * 
             */
            public function getController():?string
                {
                    return $this->controller;
                }
            
            /**
             * 
             */
            public function getMethod():?string
                {
                    return $this->method;
                }
        }
?>