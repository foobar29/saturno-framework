<?php namespace App\Models\Entities;
    use App\Core\Entity;
    /**
     * @package	Saturno
     * @author Jesús Cantú
     * @version 1.0.0.1
     */
    class Account extends Entity
        {
            private $email;
            private $password;
            private $status;

            /**
             * 
             */
            public function __construct(string $email=null, string $password=null, boolval $status=null)
                {
                    parent::__construct();
                    $this->init($email,$password,$status);
                }

            /**
             * 
             */
            public function init(string $email=null, string $password=null, bool $status=null):Account
                {
                    if($email) $this->setEmail($email);
                    if($password) $this->setPassword($password);
                    if($status) $this->setStatus($status);
                    return $this;
                }

            /**
             * 
             */
            public function setEmail(string $email)
                {
                    $this->email = $email;
                }

            /**
             * 
             */
            public function setPassword(string $password)
                {
                    $this->password = $password;
                }

            /**
             * 
             */
            public function setStatus(bool $status)
                {
                    $this->status = $status;
                }

            /**
             * 
             */
            public function getEmail():?string
                {
                    return $this->email;
                }

            /**
             * 
             */
            public function getPassword():?string
                {
                    return $this->password;
                }

            /**
             * 
             */
            public function getStatus():?bool
                {
                    return $this->status;
                }

            public function signUp(string $email=null, string $password=null, bool $status=null)
                {
                    if($email)      $this->setEmail($email);
                    if($password)   $this->setPassword($password);
                    if($status)     $this->setStatus($status); else if(!$this->getStatus()) $this->setStatus(false);

                    $req  = $this->database->prepare('call sch_sp_signUp(:email, :password, :status)');
                    $req->bindValue(':email',    $this->getEmail(),      \PDO::PARAM_STR);
                    $req->bindValue(':password', $this->getPassword(),   \PDO::PARAM_STR);
                    $req->bindValue(':status',   $this->getStatus(),     \PDO::PARAM_BOOL);
                    $req->execute();
                }
        }
?>