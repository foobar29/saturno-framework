<?php
    
    /**
     * ----------------------------------------
     * Constantes de sistema
     * ----------------------------------------
     * 
     *  Constantes consideradas por el Core de la aplicación. 
     *  En caso de aplicar, sus respectivos valores pueden ser reemplazados, aunque no es aconsejable. 
     */

    /**
     * Directorios predeterminados 
     */
    define('BASE_PATH',                 'saturno');                     // Directorio raíz del proyecto
    define('RESOURCES_PATH',            'Resources');                   // Directorio de recursos
    define('SYSTEM_PATH',               'App');                         // Directorio de sistema
    define('VIEWS_PATH',                'Views');                       // Directorio de vistas
    define('CORE_PATH',                 'Core');                        // Directorio de core
    define('LANGUAGES_PATH',            'Languages');                   // Directorio de lenguajes
    define('CONTROLLERS_PATH',          'Controllers');                 // Directorio de controladores
    //define('INTERFACES_PATH',           'Interfaces');                  // Directorio de interfaces
    //define('MODELS_PATH',               'Models');                      // Directorio de modelos
    //define('LIBRARIES_PATH',            'Libraries');                   // Directorio de librerías
    define('HELPERS_PATH',              'Helpers');                     // Directorio de helpers

    /**
     * Controladores y métodos 
     */
    define('DEF_CONTROLLER',            'Home');                        // Controlador que se carga en la URL de base
    define('DEF_METHOD',                'index');                       // Método que se accede por default
    define('DEF_CONTROLLER_WHEN_CNF',   'Error404');                    // Controlador que se carga cuando no se reconoce la URL
    define('DEF_METHOD_WHEN_MNF',       'index');                       // Método que se ejecuta cuando no se reconoce la URL
?>