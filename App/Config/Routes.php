<?php use App\{Core\Router, Models\Entities\Route};

    /**
     * ----------------------------------------
     * Rutas de acceso
     * ----------------------------------------
     * 
     *  Enlista las rutas consideradas y las vincula hacia el controlador y método que se desee ejecutar.
     * 
     *  @example
     *  $miruta = new Route('catalogo', 'busqueda', ['get', 'post']);
     *  $micontrolador = new Route('search', 'index');
     *  $router->link($miruta, $micontrolador);
     */

    $router = Router::getInstance();
    $router ->link(new Route('inicio', null,'get'), new Route('home','index'));
?>