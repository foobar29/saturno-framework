<?php use App\{Core\DatabaseConnection,Models\Entities\Credentials};
    
    /**
     * ----------------------------------------
     * Conexión a base de datos
     * ----------------------------------------
     * 
     *  Inicializa la conexión a base de datos.
     *  Es necesario que reemplaces los parámetros de acceso por los tuyos.
     */

    $host       ='localhost';
    $dbname     ='testing';
    $user       ='root';
    $password   ='';
    $dbms       ='mysql';

    $database=DatabaseConnection::getInstance();
    $database->setCredentials(new Credentials($host,$dbname,$user,$password))
             ->setDBMS($dbms);
    
    // Descomenta la línea de abajo para realizar la conexión de base de datos
    $database->connect();
?>